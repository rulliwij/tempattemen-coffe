import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import 'typeface-roboto/index.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import {
//   preset
// } from 'vue-cli-plugin-vuetify-preset-rally/preset'

Vue.use(Vuetify)

// Translation provided by Vuetify (javascript)
import id from 'vuetify/es5/locale/id'
import en from 'vuetify/es5/locale/en'

export default new Vuetify({
  // preset,
  icons: {
    iconfont: 'mdi' || 'fa', // 'mdiSvg' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
  },
  lang: {
    locales: {
      id,
      en
    },
    current: 'id',
  },
  rtl: false,
  breakpoint: {
    scrollbarWidth: 12
  },
  theme: {
    dark: false
  },
})