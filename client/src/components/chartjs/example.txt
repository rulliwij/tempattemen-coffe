noData: {
  text: 'Loading....',
  align: 'center',
  verticalAlign: 'middle',
  offsetX: 0,
  offsetY: 0,
  style: {
    fontSize: '16px',
    fontFamily: 'Helvetica, Arial, sans-serif'
  }
},
chart: {
  animations: {
    enabled: true,
    easing: "linear",
    dynamicAnimation: {
      speed: 1000
    }
  },
  toolbar: {
    show: true
  },
  zoom: {
    enabled: true
  },
  height: this.height
},
colors: this.color,
plotOptions: {
  bar: {
    horizontal: false,
    columnWidth: this.width,
    endingShape: 'rounded'
  },
},
stroke: {
  show: true,
  width: 2,
  colors: ['transparent']
},
xaxis: {
  categories: this.xAxisName,
},
yaxis: {
  labels: {
    formatter: function(val, index) {
      let price = ""
      const reverseNumber = val.toString().split("").reverse().join("")
      const arrReverseNumber = [...Array(reverseNumber.length).keys()]
      arrReverseNumber.map(index => {
        if (index % 3 === 0) price += reverseNumber.substr(index, 3) + "."
      })
      return `Rp ${
        price.split("", price.length - 1)
        .reverse()
        .join("")
      }`
    }
  },
  title: {
    text: 'Harga'
  }
},
fill: {
  opacity: 1
},
tooltip: {
  y: {
    formatter: function (val) {
      return "Rp " + val
    }
  }
},
responsive: [
  {
    breakpoint: 480,
    options: {
      chart: {
        width: 200
      },
      legend: {
        position: 'bottom'
      }
    }
  }
]