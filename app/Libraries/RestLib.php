<?php

namespace App\Libraries;

class RestLib {

    private $restConfig;
    private $request;
    private $response;

    public function __construct() {

        $this->request = \Config\Services::request();
        $this->response = \Config\Services::response();
        $this->restApi = \Config\Services::RestApi();
    }

    public function createRespond($code, $body) {
        $this->response
                // ->setHeader('Access-Control-Allow-Origin', '*')
                // ->setHeader('Access-Control-Allow-Headers', '*')
                // ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
                ->setStatusCode($code)
                ->setJSON($body)
                ->send();
        exit;
    }

    public function checkValidLogin() {
        if ($this->restApi->ipWhitelistEnabled) {
            return $this->checkWhitelistIP();
        }

        $username = $this->request->getServer('PHP_AUTH_USER');
        $password = $this->request->getServer('PHP_AUTH_PW');

        $httpAuth = $this->request->getServer('HTTP_AUTHORIZATION');

        if (empty($username) && !empty($httpAuth)) {
            if (strpos(strtolower($httpAuth), 'basic') === 0) {
                list($username, $password) = explode(':', base64_decode(substr($httpAuth, 6)));
            }
        }

        if ($this->restApi->validLoginUsername !== $username || $this->restApi->validLoginPassword !== $password) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    private function checkWhitelistIP() {
        $arrWhitelist = $this->restApi->ipWhitelist;

        array_push($arrWhitelist, '127.0.0.1', '0.0.0.0');

        foreach ($arrWhitelist as &$ip) {
            $ip = trim($ip);
        }

        if (in_array($this->request->getIPAddress(), $arrWhitelist) === FALSE) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    private function getAuthorizationHeader() {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

}
