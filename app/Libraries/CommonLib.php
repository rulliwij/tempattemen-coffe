<?php

namespace App\Libraries;

class CommonLib {

    public function __construct() {
        
    }

    public function generateToken($param) {
        return sha1($param . microtime());
    }

}
