<?php

if (!function_exists('is_json')) {

    function is_json($string) {
        return is_string($string) && is_array(json_decode($string, TRUE)) && (json_last_error() == JSON_ERROR_NONE) ? TRUE : FALSE;
    }

}

if (!function_exists('censorName')) {

    function censorName($stringName) {
        $arrWord = explode(" ", $stringName);

        foreach ($arrWord AS $key => $word) {
            if (strlen($word) > 4) {
                $arrWord[$key] = substr($word, 0, 2) . str_repeat("*", (strlen($word) - 3)) . substr($word, (strlen($word) - 1));
            } else if (strlen($word) > 2) {
                $arrWord[$key] = substr($word, 0, 2) . str_repeat("*", (strlen($word) - 2));
            }
        }

        return implode(" ", $arrWord);
    }

}

if (!function_exists('convertNullToString')) {

    function convertNullToString($v) {
        return (is_null($v)) ? "" : $v;
    }

}

if (!function_exists('nullToString')) {

    function nullToString($array) {
        if (is_array($array)) {
            return array_map("convertNullToString", $array);
        } else if (is_object($array)) {
            return (object) array_map("convertNullToString", json_decode(json_encode($array), TRUE));
        } else {
            return '';
        }
    }

}

if (!function_exists('buildFormError')) {

    function buildFormError($arrError) {
        $arrData = array();
        if (is_array($arrError)) {
            $arrayData["form"] = $arrError;
            $arrayData["formArray"] = array_column($arrError, NULL);
        }

        return $arrayData;
    }

}

if (!function_exists('regexPhone')) {

    function regexPhone($phone) {
        $phone = $phone;
        $cek_phone = preg_match("/^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}$/", $phone);
        if ($cek_phone != 1) {
            return false;
        }

        $phone = str_replace("+62", "0", $phone);
        $phone = str_replace("-", "", $phone);
        $phone = str_replace(" ", "", $phone);

        return $phone;
    }

}

if (!function_exists('isValidDate')) {

    function isValidDate($date, $format = 'Y-m-d') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

}


if (!function_exists('pageGenerator')) {

    function pageGenerator($total, $pagenum, $limit) {
        $total_page = ceil($total / $limit);

        //Prev page
        $prev = $pagenum - 1;
        if ($prev < 1) {
            $prev = 0;
        }

        //Next page
        $next = $pagenum + 1;
        if ($next > $total_page) {
            $next = 0;
        }

        $from = 1;
        $to = $total_page;

        $to_page = $pagenum - 2;
        if ($to_page > 0) {
            $from = $to_page;
        }

        if ($total_page >= 5) {
            if ($total_page > 0) {
                $to = 5 + $to_page;
                if ($to > $total_page) {
                    $to = $total_page;
                }
            } else {
                $to = 5;
            }
        }

        #looping kotak pagination
        $firstpage_istrue = false;
        $lastpage_istrue = false;
        if ($total_page <= 1) {
            $detail = [];
        } else {
            for ($i = $from; $i <= $to; $i++) {
                $detail[] = $i;
            }
            if ($from != 1) {
                $firstpage_istrue = true;
            }
            if ($to != $total_page) {
                $lastpage_istrue = true;
            }
        }

        $total_display = $limit;
        if ($next == 0) {
            $total_display = $total % $limit;
        }

        $pagination = array(
            'totalData' => $total,
            'totalPage' => $total_page,
            'totalDisplay' => $total_display,
            'firstPage' => $firstpage_istrue,
            'lastPage' => $lastpage_istrue,
            'prev' => $prev,
            'current' => $pagenum,
            'next' => $next,
            'detail' => $detail
        );

        return $pagination;
    }

}

if (!function_exists('buildWhereSearch')) {

    function buildWhereSearch($search = array(), $field_allowed = array()) {
        $sql_search = '';
        if ($search != null && isset($search['field']) && isset($search['value'])) {
            $arr_field = explode("::", $search['field']);

            $str_search = "";
            $separator = "";
            foreach ($arr_field as $field) {
                if (in_array($field, $field_allowed)) {
                    $str_search .= $separator . "`" . $field . "` LIKE '%" . $search['value'] . "%'";
                    $separator = " OR ";
                }
            }

            if (!empty($str_search)) {
                $sql_search = " AND (" . $str_search . ")";
            }
        }

        return $sql_search;
    }

}

if (!function_exists('buildWhereFilter')) {

    function buildWhereFilter($where_filter = array(), $field_allowed = array()) {
        $sql_search = '';

        if ($where_filter != null) {
            foreach ($where_filter as $row) {
                $type = isset($row['type']) ? $row['type'] : '';
                $field = isset($row['field']) ? $row['field'] : '';
                $value = isset($row['value']) ? $row['value'] : '';
                $comparison = isset($row['comparison']) ? $row['comparison'] : '';

                if (!in_array($field, $field_allowed)) {
                    $field = '';
                }

                if ($field == '' || $value == '') {
                    $type = '';
                }

                switch ($type) {
                    case 'string':
                        $arr_allowed = array('eq', 'start', 'end', 'any');
                        if (!in_array($comparison, $arr_allowed)) {
                            $comparison = '=';
                        }
                        switch ($comparison) {
                            case 'eq':
                                $sql_search .= " AND " . $field . " = '" . $value . "'";
                                break;
                            case 'start':
                                $sql_search .= " AND " . $field . " LIKE '" . $value . "%'";
                                break;
                            case 'end':
                                $sql_search .= " AND " . $field . " LIKE '%" . $value . "'";
                                break;
                            case 'any':
                                $sql_search .= " AND " . $field . " LIKE '%" . $value . "%'";
                                break;
                        }
                        break;
                    case 'numeric':
                        if (is_numeric($value)) {
                            $arr_allowed = array('eq', 'lt', 'gt', 'lte', 'gte', 'ne');
                            if (!in_array($comparison, $arr_allowed)) {
                                $comparison = '=';
                            }
                            switch ($comparison) {
                                case 'eq':
                                    $sql_search .= " AND " . $field . " = " . $value;
                                    break;
                                case 'lt':
                                    $sql_search .= " AND " . $field . " < " . $value;
                                    break;
                                case 'gt':
                                    $sql_search .= " AND " . $field . " > " . $value;
                                    break;
                                case 'lte':
                                    $sql_search .= " AND " . $field . " <= " . $value;
                                    break;
                                case 'gte':
                                    $sql_search .= " AND " . $field . " >= " . $value;
                                    break;
                                case 'ne':
                                    $sql_search .= " AND " . $field . " <> " . $value;
                                    break;
                            }
                        }
                        break;
                    case 'list':
                        if (strstr($value, '::')) {
                            $arr_allowed = array('in', 'not', 'bet');
                            if (!in_array($comparison, $arr_allowed)) {
                                $comparison = 'in';
                            }
                            $fi = explode('::', $value);
                            for ($q = 0; $q < count($fi); $q++) {
                                $fi[$q] = "'" . $fi[$q] . "'";
                            }
                            $value = implode(',', $fi);

                            switch ($comparison) {
                                case 'in':
                                    $sql_search .= " AND " . $field . " IN (" . $value . ")";
                                    break;
                                case 'not':
                                    $sql_search .= " AND " . $field . " NOT IN (" . $value . ")";
                                    break;
                                case 'bet':
                                    $sql_search .= " AND " . $field . " BETWEEN " . $fi[0] . " AND " . $fi[1];
                                    break;
                            }
                        }
                        break;
                    case 'date':
                        $value1 = '';
                        $value2 = '';
                        if (strstr($value, '::')) {
                            $date_value = explode('::', $value);
                            $value1 = $date_value[0];
                            $value2 = $date_value[1];
                        } else {
                            $value1 = $value;
                        }

                        $field = 'DATE(' . $field . ')';

                        $arr_allowed = array('eq', 'lt', 'gt', 'lte', 'gte', 'ne', 'bet');

                        if (!in_array($comparison, $arr_allowed)) {
                            $comparison = 'eq';
                        }

                        switch ($comparison) {
                            case 'eq':
                                if (isValidDate($value1)) {
                                    $sql_search .= " AND " . $field . " = '" . $value1 . "'";
                                }
                                break;
                            case 'lt':
                                if (isValidDate($value1)) {
                                    $sql_search .= " AND " . $field . " < '" . $value1 . "'";
                                }
                                break;
                            case 'gt':
                                if (isValidDate($value1)) {
                                    $sql_search .= " AND " . $field . " > '" . $value1 . "'";
                                }
                                break;
                            case 'lte':
                                if (isValidDate($value1)) {
                                    $sql_search .= " AND " . $field . " <= '" . $value1 . "'";
                                }
                                break;
                            case 'gte':
                                if (isValidDate($value1)) {
                                    $sql_search .= " AND " . $field . " >= '" . $value1 . "'";
                                }
                                break;
                            case 'ne':
                                if (isValidDate($value1)) {
                                    $sql_search .= " AND " . $field . " <> '" . $value1 . "'";
                                }
                                break;
                            case 'bet':
                                if (isValidDate($value1) && isValidDate($value2)) {
                                    $sql_search .= " AND " . $field . " BETWEEN '" . $value1 . "' AND '" . $value2 . "'";
                                }
                                break;
                        }
                        break;
                    case 'datetime':
                        $value1 = '';
                        $value2 = '';
                        if (strstr($value, '::')) {
                            $date_value = explode('::', $value);
                            $value1 = $date_value[0];
                            $value2 = $date_value[1];
                        } else {
                            $value1 = $value;
                        }

                        $arr_allowed = array('eq', 'lt', 'gt', 'lte', 'gte', 'ne', 'bet');

                        if (!in_array($comparison, $arr_allowed)) {
                            $comparison = 'eq';
                        }

                        switch ($comparison) {
                            case 'eq':
                                if (isValidDate($value1, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " = '" . $value1 . "'";
                                }
                                break;
                            case 'lt':
                                if (isValidDate($value1, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " < '" . $value1 . "'";
                                }
                                break;
                            case 'gt':
                                if (isValidDate($value1, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " > '" . $value1 . "'";
                                }
                                break;
                            case 'lte':
                                if (isValidDate($value1, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " <= '" . $value1 . "'";
                                }
                                break;
                            case 'gte':
                                if (isValidDate($value1, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " >= '" . $value1 . "'";
                                }
                                break;
                            case 'ne':
                                if (isValidDate($value1, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " <> '" . $value1 . "'";
                                }
                                break;
                            case 'bet':
                                if (isValidDate($value1, 'Y-m-d H:i:s') && isValidDate($value2, 'Y-m-d H:i:s')) {
                                    $sql_search .= " AND " . $field . " BETWEEN '" . $value1 . "' AND '" . $value2 . "'";
                                }
                                break;
                        }
                        break;
                }
            }
        }

        return $sql_search;
    }

}