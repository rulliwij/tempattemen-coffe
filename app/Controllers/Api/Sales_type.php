<?php

namespace App\Controllers\Api;

class Sales_type extends \App\Controllers\ApiAuthUserController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }
    
    public function list() {
        $table = "sales_type";

        $defaultSort = "sales_type_name";
        $defaultDir = "ASC";

        $arrField = array(
            'sales_type_id',
            'sales_type_name',
            'sales_type_slug',
            'sales_type_margin',
            'sales_type_value'
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function create(){
        $this->validation->setRule('sales_type_name', 'Nama', 'required');
        $this->validation->setRule('sales_type_margin', 'Type Margin', 'required');
        $this->validation->setRule('sales_type_value', 'Margin Value', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $name = $this->request->getPost('sales_type_name');
        $margin = $this->request->getPost('sales_type_margin');
        $value = $this->request->getPost('sales_type_value');
        $slug = str_replace(' ', '_', strtolower($name));

        $check_name = $this->db->table('sales_type')->select('sales_type_slug')->getWhere(['sales_type_slug' => $slug])->getRow('sales_type_slug');
        if (!empty($check_name)) {
            $this->respondFailed("Nama tipe penjualan sudah ada.");
        }

        $arr_data = [];
        $arr_data['sales_type_name'] = $name;
        $arr_data['sales_type_slug'] = $slug;
        $arr_data['sales_type_margin'] = $margin;
        $arr_data['sales_type_value'] = $value;
        if (!$this->db->table('sales_type')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan tipe penjualan.");
        }
        
        $this->respondSuccess("Berhasil menambahkan tipe penjualan.");
    }

    public function update(){
        $this->validation->setRule('sales_type_name', 'Nama', 'required');
        $this->validation->setRule('sales_type_margin', 'Type Margin', 'required');
        $this->validation->setRule('sales_type_value', 'Margin Value', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $id = $this->request->getPost('sales_type_id');
        $name = $this->request->getPost('sales_type_name');
        $slug = str_replace(' ', '_', $name);
        $margin = $this->request->getPost('sales_type_margin');
        $value = $this->request->getPost('sales_type_value');

        $check_name = $this->db->table('sales_type')->select('sales_type_slug')->getWhere(['sales_type_slug' => $slug])->getRow('sales_type_slug');
        // if (!empty($check_name)) {
        //     $this->respondFailed("Nama tipe penjualan sudah ada.");
        // }

        if ($id == 0) {
            $this->respondFailed("Tipe penjualan tidak ditemukan.");
        }

        $check_id = $this->db->table('sales_type')->select('sales_type_id')->getWhere(['sales_type_id' => $id])->getRow('sales_type_id');
        if(empty($check_id)) {
            $this->respondFailed("Tipe penjualan tidak ditemukan.");
        }

        $arr_data = [];
        $arr_data['sales_type_name'] = $name;
        $arr_data['sales_type_slug'] = $slug;
        $arr_data['sales_type_margin'] = $margin;
        $arr_data['sales_type_value'] = $value;
        $this->db->table('sales_type')->where('sales_type_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui tipe penjualan.");
        }
        $this->respondSuccess("Berhasil memperbarui tipe penjualan.");
    }

    public function delete(){
        $id = $this->request->getPost('sales_type_id');
        if ($id == 0) {
            $this->respondFailed("Tipe penjualan tidak ditemukan.");
        }

        $check_id = $this->db->table('sales_type')->select('sales_type_id, sales_type_name')->getWhere(['sales_type_id' => $id])->getRow();
        $dataRes = array('salesName' => $check_id->sales_type_name);
        if(empty($check_id)) {
            $this->respondFailed("Tipe penjualan tidak ditemukan.");
        }
        $this->db->table('sales_type')->where('sales_type_id', $id)->delete();
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal menghapus tipe penjualan.");
        }

        $this->respondSuccess("Berhasil menghapus tipe penjualan.", $dataRes);
    }
}
