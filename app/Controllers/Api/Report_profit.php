<?php

namespace App\Controllers\Api;

class Report_profit extends \App\Controllers\ApiAuthUserController {
  
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
		parent::initController($request, $response, $logger);
	}

	public function summary()
	{
        $start_date = $this->request->getGet('start');
        $end_date = $this->request->getGet('end');
        if(empty($start_date) && empty($end_date)) {
            $where_modal = "";
            $where_pengeluaran = "";
            $where_pendapatan = "WHERE sales_order_status = 'complete'";
            $where_bahan_baku = "WHERE po_is_product = 1";
            $where_non_bahan_baku = "WHERE po_is_product = 0";
        } else {
            $where_modal = "WHERE DATE(cashier_cash_input_datetime) BETWEEN '".$start_date."' AND '".$end_date."'";
            $where_pengeluaran = "WHERE DATE(po_input_datetime) BETWEEN '".$start_date."' AND '".$end_date."'";
            $where_pendapatan = "WHERE sales_order_status = 'complete' AND DATE(sales_order_input_datetime) BETWEEN '".$start_date."' AND '".$end_date."'";
            $where_bahan_baku = "WHERE po_is_product = 1 AND DATE(po_input_datetime) BETWEEN '".$start_date."' AND '".$end_date."'";
            $where_non_bahan_baku = "WHERE po_is_product = 0 AND DATE(po_input_datetime) BETWEEN '".$start_date."' AND '".$end_date."'";
        }

        $sql_modal = "
            SELECT SUM(cashier_cash_in) AS modal_in,
            SUM(cashier_cash_out) AS modal_out
            FROM cashier_cash
            $where_modal
        ";
        $data_modal = $this->db->query($sql_modal)->getRow();
        if (!empty($data_modal->modal_in)) {
            $modal_in = $data_modal->modal_in;
        } else {
            $modal_in = 0;
        }
        if (!empty($data_modal->modal_out)) {
            $modal_out = $data_modal->modal_out;
        } else {
            $modal_out = 0;
        }

        $sql_bahan = "SELECT SUM(po_grand_total) AS bahan FROM po $where_bahan_baku";
        $data_bahan = $this->db->query($sql_bahan)->getRow('bahan');
        if (!empty($data_bahan)) {
            $bahan = $data_bahan;
        } else {
            $bahan = 0;
        }

        $sql_non_bahan = "SELECT SUM(po_grand_total) AS non_bahan FROM po $where_non_bahan_baku";
        $data_non_bahan = $this->db->query($sql_non_bahan)->getRow('non_bahan');
        if (!empty($data_non_bahan)) {
            $non_bahan = $data_non_bahan;
        } else {
            $non_bahan = 0;
        }

        $sql_pengeluaran = "SELECT SUM(po_grand_total) AS pengeluaran FROM po $where_pengeluaran";
        $data_pengeluaran = $this->db->query($sql_pengeluaran)->getRow('pengeluaran');
        if (!empty($data_pengeluaran)) {
            $pengeluaran = $data_pengeluaran;
        } else {
            $pengeluaran = 0;
        }

        $sql_pendapatan = " SELECT SUM(sales_order_grand_total) AS pendapatan FROM sales_order $where_pendapatan";
        $data_pendapatan = $this->db->query($sql_pendapatan)->getRow('pendapatan');
        if (!empty($data_pendapatan)) {
            $pendapatan = $data_pendapatan;
        } else {
            $pendapatan = 0;
        }

        $data = array(
            'summary' => array(
                'modal_in' => $modal_in,
                'modal_out' => $modal_out,
                'pendapatan' => $pendapatan,
                'bahan' => $bahan,
                'non_bahan' => $non_bahan,
                'pengeluaran' => $pengeluaran,
                'cash_in' => $pendapatan + $modal_in,
                'cash_out' => $pengeluaran + $modal_in,
                'profit' => ($pendapatan + $modal_in) - ($pengeluaran + $modal_in),
            )
        );
        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

}