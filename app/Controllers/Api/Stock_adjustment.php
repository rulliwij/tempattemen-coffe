<?php

namespace App\Controllers\Api;

class Stock_adjustment extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }

  public function list() {
    $table = "stock_adjustment";
    $defaultSort = "stock_adjustment_id";
    $defaultDir = "DESC";

    $arrField = array(
        'stock_adjustment_id',
        'stock_adjustment_store_id',
        'stock_adjustment_product_id',
        'stock_adjustment_product_code',
        'stock_adjustment_product_name',
        'stock_adjustment_system_qty',
        'stock_adjustment_qty',
        'stock_adjustment_after_qty',
        'stock_adjustment_note',
        'stock_adjustment_input_datetime',
        'stock_adjustment_user_id',
        'stock_adjustment_user_username',
        'stock_adjustment_user_fullname'
    );

    $where = "stock_adjustment_store_id = '{$this->user->user_auth_user_store_id}'";
    $join = "";

    $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
    $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

    $search = (array) $this->request->getGet('search');
    $filter = (array) $this->request->getGet('filter');
    $sort = (string) $this->request->getGet('sort');
    $dir = (string) strtoupper($this->request->getGet('dir'));

    if ($dir !== 'ASC' && $dir !== 'DESC') {
        $dir = $defaultDir;
    }

    $start = ($page - 1) * $limit;

    $joinDetail = empty($join) ? "" : $join;
    $whereDetail = empty($where) ? " 1 = 1 " : $where;

    if (($keyDelete = array_search('stock_adjustment_after_qty', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (is_array($search)) {
        $whereDetail .= buildWhereSearch($search, $arrField);
    }

    if (is_array($filter)) {
        $whereDetail .= buildWhereFilter($filter, $arrField);
    }

    if (!in_array($sort, $arrField)) {
        $sort = $defaultSort;
    }

    $strField = empty($arrField) ? '*' : implode(',', $arrField);

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        (stock_adjustment_system_qty + stock_adjustment_qty) AS stock_adjustment_after_qty,
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";

    $queryResult = $this->db->query($sql);

    $totalData = 0;
    $dataResult = array();

    if ($queryResult->resultID->num_rows > 0) {

        $sqlTotal = "SELECT FOUND_ROWS() AS row";

        $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

        $result = $queryResult->getResult();

        foreach ($result as $row) {
            $dataResult[] = nullToString($row);
        }
    }

    $data = array(
        'data' => $dataResult,
        'pagination' => pageGenerator($totalData, $page, $limit)
    );

    $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }

  public function create() {
    $items = json_decode($this->request->getPost('items'));

    $datetime = date('Y-m-d H:i:s');

    $respond = array();

    $isError = FALSE;
    $msgError = "";

    $this->db->transBegin();

    try {
        $tableStockAdjustment = $this->db->table('stock_adjustment');
        $tableStockLog = $this->db->table('stock_log');
        $tableStock = $this->db->table('stock');

        foreach($items as $key => $value){
            $sql = "
                SELECT * FROM product
                JOIN stock ON stock_product_id = product_id
                WHERE product_id = '{$value->product_id}' AND stock_product_store_id = '{$this->user->user_auth_user_store_id}'
            ";

            $product = $this->db->query($sql)->getRow();

            $arr_data = [];
            $arr_data['stock_adjustment_store_id'] = $this->user->user_auth_user_store_id;
            $arr_data['stock_adjustment_product_id'] = $value->product_id;
            $arr_data['stock_adjustment_product_code'] = $product->product_code;
            $arr_data['stock_adjustment_product_name'] = $product->product_name;
            $arr_data['stock_adjustment_system_qty'] = $product->stock_balance;
            $arr_data['stock_adjustment_qty'] = $value->qty;
            $arr_data['stock_adjustment_note'] = $value->note;
            $arr_data['stock_adjustment_input_datetime'] = $datetime;
            $arr_data['stock_adjustment_user_id'] = $this->user->user_auth_user_id;
            $arr_data['stock_adjustment_user_username'] = $this->user->user_username;
            $arr_data['stock_adjustment_user_fullname'] = $this->user->user_fullname;

            if (!$tableStockAdjustment->insert($arr_data)) {
                throw new Exception("Gagal insert stock adjustment");
            }

            $movement_type = 1;
            
            if ($value->qty < 0) {
                $movement_type = 2;
            }
            
            $arr_stock_log = [];
            $arr_stock_log['stock_log_store_id'] = $this->user->user_auth_user_store_id;
            $arr_stock_log['stock_log_code'] = $this->generateCode("stock_log", "stock_log_code" , "WHERE stock_log_transaction_type = 3");
            $arr_stock_log['stock_log_product_id'] = $value->product_id;
            $arr_stock_log['stock_log_product_category_name'] = $product->product_category_name;
            $arr_stock_log['stock_log_product_code'] = $product->product_code;
            $arr_stock_log['stock_log_product_name'] = $product->product_name;
            $arr_stock_log['stock_log_qty'] = abs($value->qty);
            $arr_stock_log['stock_log_movement_type'] = $movement_type;
            $arr_stock_log['stock_log_transaction_type'] = 3;
            $arr_stock_log['stock_log_note'] = "[Penyesuaian] " . $value->note;
            $arr_stock_log['stock_log_input_datetime'] = $datetime;
            $arr_stock_log['stock_log_input_user_id'] = $this->user->user_auth_user_id;
            $arr_stock_log['stock_log_input_user_username'] = $this->user->user_username;
            $arr_stock_log['stock_log_input_user_fullname'] = $this->user->user_fullname;
            $arr_stock_log['stock_log_product_purchase_price'] = 0;
            $arr_stock_log['stock_log_product_sales_price'] = 0;

            if (!$tableStockLog->insert($arr_stock_log)) {
                throw new Exception("Gagal insert stock log");
            }

            $operator = "+";

            if($value->qty < 0){
                $operator = "-";
            }
            
            $tableStock->set('stock_balance', 'stock_balance' . $operator . abs($value->qty), FALSE);
            $tableStock->where('stock_product_id', $value->product_id);
            $tableStock->where('stock_product_store_id', $this->user->user_auth_user_store_id);
            $tableStock->update();
            if ($this->db->affectedRows() < 0) {
                 throw new Exception("Gagal update stock");
            }
        }
    } catch (Exception $exc) {
        $isError = TRUE;
        $msgError = $exc->getMessage();
    }

    if ($this->db->transStatus() === FALSE || $isError === TRUE) {
        $this->db->transRollback();
        $this->respondFailed("Gagal menyesuaikan stok. " . $msgError);
    }

    $this->db->transCommit();
    $this->respondSuccess("Penyesuaian stok berhasil diproses.", $respond);
  }

    public function product_options() {
        $sql = "
            SELECT product_id, product_code, product_name, product_unit, stock_balance
            FROM product
            JOIN stock ON stock_product_id = product_id
            WHERE product_is_deleted = 0 AND stock_product_store_id = '{$this->user->user_auth_user_store_id}'
        ";

        $product = $this->db->query($sql)->getResultArray();

        $dataProduct = [];

        foreach ($product as $row) {
            $row['qty'] = 0;
            $row['note'] = "";

            $dataProduct[] = nullToString($row);
        }

        $data = array(
            "data" => $dataProduct
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }
    

    function generateCode($table_name = '', $fieldname = '', $extra = '', $digit = 5) {
        $sql = "
            SELECT
            IFNULL(LPAD(MAX(CAST(RIGHT(" . $fieldname . ", " . $digit . ") AS SIGNED) + 1), " . $digit . ", '0'), '" . sprintf('%0' . $digit . 'd', 1) . "') AS code 
            FROM " . $table_name . "
            " . $extra . "
          ";
        $query = $this->db->query($sql);

        if ($query->resultID->num_rows > 0) {
            $row = $query->getRow();
            return $row->code;
        } else {
            return '';
        }
    }

}