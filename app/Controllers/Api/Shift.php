<?php

namespace App\Controllers\Api;

class Shift extends \App\Controllers\ApiAuthUserController {
  
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
		parent::initController($request, $response, $logger);
	}
	
	public function shift_list(){
		$limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

		$start = ($page - 1) * $limit;

		$strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
		}

		// Get Close Register
		$sql_close_register = "SELECT * FROM close_register $strLimit";
		$get_close_register = $this->db->query($sql_close_register)->getResult();
		
		// Pagination
		$totalData = 0;

		$sql_close_register = "SELECT close_register_id FROM close_register";
		$get_close_register_total = $this->db->query($sql_close_register);
		if ($get_close_register_total->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;
        }
		
		$no = 1;
		$cashier_cash_index = 0;
		foreach($get_close_register as $close_register){
			nullToString($close_register);
			
			// Get Cashier Cash
			$sql_cashier_cash = "SELECT * FROM cashier_cash WHERE cashier_cash_input_datetime < '{$close_register->close_register_input_datetime}' AND cashier_cash_input_datetime AND cashier_cash_input_user_id = {$close_register->close_register_input_user_id} ORDER BY cashier_cash_input_datetime";
			
			$get_cashier_cash = $this->db->query($sql_cashier_cash)->getResult();
			
			// Get Sales Order
			$first_date = $get_cashier_cash[$cashier_cash_index]->cashier_cash_input_datetime;
			$second_date = $close_register->close_register_input_datetime;

			$date_range = "'{$first_date}' AND '{$second_date}'";
			
			$sql_so = "SELECT * FROM sales_order WHERE sales_order_input_datetime BETWEEN {$date_range}";
			
			$get_so = $this->db->query($sql_so)->getResult();
			
			foreach($get_so as $so){
				nullToString($so);
			}
			
			$close_register->detail = $get_so;
			$close_register->shift_start_date = $first_date;
			$cashier_cash_index = count($get_cashier_cash);

		}
		$result = $get_close_register;
		
		$data = array(
			'data' => $result,
			'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
	}
}