<?php

namespace App\Controllers\Api;

class CdnController extends \App\Controllers\BaseApiController {

    public $user = array();

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }

    public function upload_image() {
        $this->validation->setRule('img', 'Gambar', 'uploaded[img]|max_size[img,5120]|ext_in[img,png,jpg,jpeg,csv]');

        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form yang Kamu masukkan.", $errorData);
        }

        $img = $this->request->getFile('img');

        if (!$img->isValid() || $img->hasMoved()) {
            $this->respondFailed("Gagal mengupload gambar.");
        }

//        $newName = $img->getRandomName();
        $newName = $img->getExtension() == 'csv' ? time() . '_' . bin2hex(random_bytes(10)) . '.jpg' : $img->getRandomName();

        if (!$img->move(FCPATH . 'media/images', $newName)) {
            $this->respondFailed("Gagal mengupload gambar.");
        }

        $imgData = @exif_read_data(FCPATH . 'media/images/' . $newName, 'IFD0');

        if ($imgData !== false && in_array("Orientation", $imgData)) {
            $rotation = 0;
            switch ($imgData['Orientation']) {
                case 3:
                    $rotation = 180;
                    break;
                case 6:
                    $rotation = 270;
                    break;
                case 8:
                    $rotation = 90;
                    break;
            }
            
            if ($rotation !== 0) {
                $image = \Config\Services::image();
                
                $image->withFile(FCPATH . 'media/images/' . $newName)
                        ->rotate($rotation)
                        ->save(FCPATH . 'media/images/' . $newName);
            }
        }

        $respond = array(
            'filename' => $newName,
            'path' => base_url('/media/images/' . $newName)
        );

        $this->respondSuccess("Berhasil mengupload gambar.", $respond);
    }
}