<?php

namespace App\Controllers\Api;

class Report_sales_order extends \App\Controllers\ApiAuthUserController {
  
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
		parent::initController($request, $response, $logger);
	}

	public function list()
	{
        $start_date = $this->request->getGet('start');
        $end_date = $this->request->getGet('end');
        $limit = (int) $this->request->getGet('limit') <= 0 ? 10 : (int) $this->request->getGet('limit');
        $page = (int) $this->request->getGet('page') <= 0 ? 1 : (int) $this->request->getGet('page');
        $sort = (string) $this->request->getGet('sort');
        if(empty($sort)){
            $sort = 'datetime';
        }
        $dir = strtoupper($this->request->getGet('dir'));
        if ($dir != 'ASC' && $dir != 'DESC') {
            $dir = 'DESC';
        }
        
        $start = ($page - 1) * $limit;

		if(empty($start_date) && empty($end_date)) {
			$start_date = date('Y-m-01');
			$end_date = date('Y-m-t');
        }
        
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS * FROM (
                SELECT sales_order_id AS id,
                sales_order_date AS date, 
                sales_order_input_datetime AS datetime,
                sales_order_total_product_price AS product_price,
                sales_order_total_discount_item_nominal AS nominal_discount,
                sales_order_total_nett_price AS nett_price,
                sales_order_grand_total AS grand_total
                FROM sales_order
                WHERE sales_order_date BETWEEN '".$start_date."' AND '".$end_date."' 
                AND sales_order_status = 'complete'
            ) result
            ORDER BY $sort $dir
            LIMIT $start, $limit
        ";

        $data = $this->db->query($sql)->getResultArray();
        $arr_result = array();
        $arr_summary = array(
            'gross_sales' => 0,
            'discount_sales' => 0,
            'nett_sales' => 0,
        );

        
        $totalData = 0;
		if (!empty($data)) {
            $sqlTotal = "SELECT FOUND_ROWS() AS row";
            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;
            $sql_summary = "
                SELECT SUM(sales_order_total_product_price) AS gross_sales,
                SUM(sales_order_total_discount_item_nominal) AS discount_sales,
                SUM(sales_order_grand_total) AS nett_sales
                FROM sales_order
                WHERE sales_order_date BETWEEN '".$start_date."' AND '".$end_date."' 
                AND sales_order_status = 'complete'
            ";
            
            $data_summary = $this->db->query($sql_summary)->getRow();
            // print_r($data_summary);die;
            if (!empty($data_summary)) {
                $arr_summary = array(
                    'gross_sales' => $data_summary->gross_sales,
                    'discount_sales' => $data_summary->discount_sales,
                    'nett_sales' => $data_summary->nett_sales,
                );
            }
            foreach ($data as $key => $row) {
                // $arr_summary['gross_sales'] += $row['product_price'];
                // $arr_summary['discount_sales'] += $row['nominal_discount'];
                // $arr_summary['nett_sales'] += $row['grand_total'];
                $sql_detail = "
                    SELECT sales_order_detail_id AS detail_id,
                    sales_order_detail_product_name AS name, sales_order_detail_product_qty AS qty,
                    sales_order_detail_product_selling_price AS price,
                    sales_order_detail_product_discount_nominal AS discount,
                    sales_order_detail_product_subtotal_price AS nett_price
                    FROM sales_order_detail
                    WHERE sales_order_detail_sales_order_id = {$row['id']}
                ";
                $data_detail = $this->db->query($sql_detail)->getResultArray();
                $row['detail'] = $data_detail;
                if (!isset($arr_result[$row['datetime']])) {
                    $arr_result['report'][] = $row;
                } else {
                    array_push($arr_result[$row['datetime']], $row);
                }
			}
        }
        
        $data = array(
            'summary' => $arr_summary,
            'data' => $arr_result,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
	}
}