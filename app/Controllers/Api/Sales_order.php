<?php

namespace App\Controllers\Api;

class Sales_order extends \App\Controllers\BaseApiController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "sales_order";

        $defaultSort = "sales_order_id";
        $defaultDir = "DESC";

        $arrField = array(
            'sales_order_id',
            'sales_order_store_id',
            'sales_order_number',
            'sales_order_transaction_number',
            'sales_order_table_number',
            'sales_order_note',
            'sales_order_payment_method',
            'sales_order_payment_bank_name',
            'sales_order_payment_ref_number',
            'sales_order_date',
            'sales_order_total_product_qty',
            'sales_order_total_product_price',
            'sales_order_total_discount_percent',
            'sales_order_total_discount_nominal',
            'sales_order_total_discount_item_nominal',
            'sales_order_total_nett_price',
            'sales_order_total_charge_percent',
            'sales_order_total_charge_nominal',
            'sales_order_total_tax_percent',
            'sales_order_total_tax_nominal',
            'sales_order_grand_total',
            'sales_order_customer_id',
            'sales_order_customer_name',
            'sales_order_input_datetime',
            'sales_order_input_user_id',
            'sales_order_input_user_fullname',
            'sales_order_input_user_username',
            'sales_order_status',
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $row->detail = $this->get_detail($row->sales_order_id);
                $dataResult[] = nullToString($row);
            }
        }

        $sql_summary = "
        SELECT
            SUM(sales_order_total_product_qty) AS summary_qty,
            SUM(sales_order_total_product_price) AS summary_product_price,
            SUM(sales_order_total_discount_item_nominal) AS summary_discount,
            SUM(sales_order_total_nett_price) AS summary_nett_price,
            SUM(sales_order_grand_total) AS summary_grand_total
        FROM
            sales_order";
        
        $summary = nullToString($this->db->query($sql_summary)->getRow());

        $data = array(
            'data' => $dataResult,
            'summary' => $summary,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function refund(){
        $this->validation->setRule('sales_order_id', 'Id', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $id = $this->request->getPost('id');
        
        if ($id == 0) {
            $this->respondFailed("Sales Order tidak ditemukan.");
        }

        $check_id = $this->db->table('sales_order')->select('sales_order_id')->getWhere(['sales_order_id' => $id, 'sales_order_status' => 'complete'])->getRow('sales_order_id');
        if(empty($check_id)) {
            $this->respondFailed("Sales Order tidak ditemukan.");
        }

        $check_sift = $this->db->table('close_register')->select('close_register_input_user_id')->getWhere(['close_register_input_user_id' => $this->user->user_auth_user_id])->getRow('sales_order_id');
        if(!empty($check_sift)) {
            $this->respondFailed("Refund produk tidak bisa dilakukan diluar shift anda");
        }

        $arr_data = [];
        $arr_data['sales_order_status'] = 'refund';
        $this->db->table('sales_order')->where('sales_order_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui sales order.");
        }
        $this->respondSuccess("Berhasil memperbarui sales order.");
    }

    public function list_daily(){
        $date = empty($this->request->getGet('date')) ? date('Y-m-d') : $this->request->getGet('date');

        $table = "sales_order";

        $defaultSort = "sales_order_id";
        $defaultDir = "DESC";

        $arrField = array(
            'sales_order_id',
            'sales_order_store_id',
            'sales_order_number',
            'sales_order_transaction_number',
            'sales_order_table_number',
            'sales_order_note',
            'sales_order_payment_method',
            'sales_order_payment_bank_name',
            'sales_order_payment_ref_number',
            'sales_order_date',
            'sales_order_total_product_qty',
            'sales_order_total_product_price',
            'sales_order_total_discount_percent',
            'sales_order_total_discount_nominal',
            'sales_order_total_discount_item_nominal',
            'sales_order_total_nett_price',
            'sales_order_total_charge_percent',
            'sales_order_total_charge_nominal',
            'sales_order_total_tax_percent',
            'sales_order_total_tax_nominal',
            'sales_order_grand_total',
            'sales_order_customer_id',
            'sales_order_customer_name',
            'sales_order_input_datetime',
            'sales_order_input_user_id',
            'sales_order_input_user_fullname',
            'sales_order_input_user_username',
            'sales_order_status',
        );

        $where = "sales_order_date = '".$date."'";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $row->detail = $this->get_detail($row->sales_order_id);
                $dataResult[] = nullToString($row);
            }
        }

        $sql_summary = "
        SELECT
            SUM(sales_order_total_product_qty) AS summary_qty,
            SUM(sales_order_total_product_price) AS summary_product_price,
            SUM(sales_order_total_discount_item_nominal) AS summary_discount,
            SUM(sales_order_total_nett_price) AS summary_nett_price,
            SUM(sales_order_grand_total) AS summary_grand_total
        FROM
            sales_order
        WHERE {$where}";
        
        $summary = nullToString($this->db->query($sql_summary)->getRow());

        $data = array(
            'data' => $dataResult,
            'summary' => $summary,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function list_monthly(){
        $month = empty($this->request->getGet('month')) ? date('m') : $this->request->getGet('month');

        $table = "sales_order";

        $defaultSort = "sales_order_id";
        $defaultDir = "DESC";

        $arrField = array(
            'sales_order_id',
            'sales_order_store_id',
            'sales_order_number',
            'sales_order_transaction_number',
            'sales_order_table_number',
            'sales_order_note',
            'sales_order_payment_method',
            'sales_order_payment_bank_name',
            'sales_order_payment_ref_number',
            'sales_order_date',
            'sales_order_total_product_qty',
            'sales_order_total_product_price',
            'sales_order_total_discount_percent',
            'sales_order_total_discount_nominal',
            'sales_order_total_discount_item_nominal',
            'sales_order_total_nett_price',
            'sales_order_total_charge_percent',
            'sales_order_total_charge_nominal',
            'sales_order_total_tax_percent',
            'sales_order_total_tax_nominal',
            'sales_order_grand_total',
            'sales_order_customer_id',
            'sales_order_customer_name',
            'sales_order_input_datetime',
            'sales_order_input_user_id',
            'sales_order_input_user_fullname',
            'sales_order_input_user_username',
            'sales_order_status',
        );

        $where = "MONTH(sales_order_date) = '".$month."'";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $row->detail = $this->get_detail($row->sales_order_id);
                $dataResult[] = nullToString($row);
            }
        }

        $sql_summary = "
        SELECT
            SUM(sales_order_total_product_qty) AS summary_qty,
            SUM(sales_order_total_product_price) AS summary_product_price,
            SUM(sales_order_total_discount_item_nominal) AS summary_discount,
            SUM(sales_order_total_nett_price) AS summary_nett_price,
            SUM(sales_order_grand_total) AS summary_grand_total
        FROM
            sales_order
        WHERE {$where}";
        
        $summary = nullToString($this->db->query($sql_summary)->getRow());

        $data = array(
            'data' => $dataResult,
            'summary' => $summary,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function get_detail($sales_order_id){
        $sql = "SELECT sales_order_detail_id, sales_order_detail_sales_order_id, sales_order_detail_category_id, sales_order_detail_category_name, sales_order_detail_product_id, sales_order_detail_product_code, sales_order_detail_product_name, sales_order_detail_product_qty, sales_order_detail_product_unit, sales_order_detail_product_purchase_price, sales_order_detail_product_selling_price, sales_order_detail_product_discount_percent, sales_order_detail_product_discount_nominal, sales_order_detail_product_nett_price, sales_order_detail_product_subtotal_price, sales_order_detail_note
            FROM sales_order_detail
            WHERE sales_order_detail_sales_order_id = {$sales_order_id}
        ";
        return $this->db->query($sql)->getResult();
    }

    public function delete(){
        $id = $this->request->getPost('id');
        if ($id == 0) {
            $this->respondFailed("Sales Order tidak ditemukan.");
        }

        $check_id = $this->db->table('sales_order')->select('sales_order_id, sales_order_table_number')->getWhere(['sales_order_id' => $id])->getRow();
        $dataRes = array('tableNumber' => $check_id->sales_order_table_number);
        if(empty($check_id)) {
            $this->respondFailed("Sales Order tidak ditemukan.");
        }

        // delete sales order detail
        $this->db->table('sales_order_detail')->where('sales_order_detail_sales_order_id', $id)->delete();
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal Hapus Sales Order");
        }

        // delete sales order
        $this->db->table('sales_order')->where('sales_order_id', $id)->delete();
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal Hapus Sales Order");
        }

        $this->respondSuccess("Berhasil menghapus Sales Order", $dataRes);
    }
}