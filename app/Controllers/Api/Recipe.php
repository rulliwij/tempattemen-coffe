<?php

namespace App\Controllers\Api;

class Recipe extends \App\Controllers\ApiAuthUserController {
  
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
		parent::initController($request, $response, $logger);
	}

	public function list(){
		$table = "product_composition";

        $defaultSort = "product_composition_id";
        $defaultDir = "ASC";

        $arrField = array(
            'product_composition_master_id',
            'COUNT(product_composition_master_id) AS total_recipe',
			'product_composition_input_datetime',
			'product_composition_input_user_fullname',
			'product_name'
        );

        $where = "";
        $join = "JOIN product ON product_id = product_composition_master_id";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            GROUP BY product_composition_master_id
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $sql_get_product_recipe = "
                    SELECT 
                        product_composition_recipe_id,
                        product_composition_qty,
                        product_composition_unit,
                        product_composition_note,
                        product_name,
                        product_category_id,
                        product_id
                    FROM product_composition
                    JOIN product ON product_id = product_composition_recipe_id
                    WHERE product_composition_master_id = '{$row->product_composition_master_id}'";
                    
                $get_product_recipe = $this->db->query($sql_get_product_recipe)->getResult();
                $row->recipe_detail = $get_product_recipe;
                
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
	}

    public function detail(){
        $productId = $this->request->getGet('productId');
        
        $table = "product_composition";

        $defaultSort = "product_composition_id";
        $defaultDir = "ASC";

        $arrField = array(
            'product_composition_recipe_id',
            'product_composition_qty',
			'product_composition_unit',
			'product_composition_note',
			'product_name'
        );

        $where = "product_composition_master_id = '{$productId}'";
        $join = "JOIN product ON product_id = product_composition_recipe_id";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
	}

	public function product_list(){
		$table = "product";

        $defaultSort = "product_id";
        $defaultDir = "DESC";

        $arrField = array(
            'product_id', 
            'product_category_id', 
            'product_category_name', 
            'product_name', 
            'product_unit', 
        );

        $where = "product_is_deleted = 0 AND product_type = 1 AND product_is_have_composition = 0";
		
        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        WHERE {$where}
        ";

        $queryResult = $this->db->query($sql);

        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
        );
        
        $this->respondSuccess("Berhasil mendapatkan data.", $data);
	}

	public function product_recipe_list(){
		$table = "product";

        $defaultSort = "product_id";
        $defaultDir = "DESC";

        $arrField = array(
            'product_id', 
            'product_code', 
            'product_category_id', 
            'product_category_name', 
            'product_name', 
            'product_unit', 
            'product_purchase_price', 
        );

        $where = "product_is_deleted = 0 AND product_type = 0";
		
        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        WHERE {$where}
        ";

        $queryResult = $this->db->query($sql);

        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
        );
        
        $this->respondSuccess("Berhasil mendapatkan data.", $data);
	}

	public function create(){
		$this->validation->setRule('productId', 'Produk', 'required');
        $this->validation->setRule('productJson', 'Produk Resep', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
		}
		
		$product_id = $this->request->getPost('productId');
		$product_json = json_decode($this->request->getPost('productJson'));
		$user_id = $this->user->user_auth_user_id;
		$data_user = $this->db->table('user')->select('*')->getWhere(['user_id' => $user_id])->getRow();

        if(empty($product_json)){
            $this->respondFailed("Bahan baku tidak boleh kosong.");
        }

		if(empty($data_user)){
			$this->respondFailed("Data user tidak ditemukan.");
		}

		$username = $data_user->user_username;
		$user_fullname = $data_user->user_fullname;
		$datetime = date('Y-m-d H:i:s');

		foreach($product_json as $product_recipe){
			$product_recipe_qty = $product_recipe->qty;
			$product_recipe_unit = $product_recipe->unit;
			$product_recipe_id = $product_recipe->recipe_id;
			$note = htmlspecialchars($product_recipe->note);

			$arr_data = [];
			$arr_data['product_composition_master_id'] = $product_id;
			$arr_data['product_composition_recipe_id'] = $product_recipe_id;
			$arr_data['product_composition_qty'] = $product_recipe_qty;
			$arr_data['product_composition_unit'] = $product_recipe_unit;
			$arr_data['product_composition_note'] = $note;
			$arr_data['product_composition_input_datetime'] = $datetime;
			$arr_data['product_composition_input_user_id'] = $user_id;
			$arr_data['product_composition_input_user_username'] = $username;
			$arr_data['product_composition_input_user_fullname'] = $user_fullname;

			if (!$this->db->table('product_composition')->insert($arr_data)) {
				$this->respondFailed("Gagal menambahkan resep.");
			}
		}

		$arr_data_product = [];
		$arr_data_product['product_is_have_composition'] = 1;
		$this->db->table('product')->where('product_id', $product_id)->update($arr_data_product);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui produk.");
        }

		$this->respondSuccess("Berhasil menambahkan resep.");
	}

	public function update(){
		$this->validation->setRule('productId', 'Produk', 'required');
        $this->validation->setRule('productJson', 'Produk Resep', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
		}

		$product_id = $this->request->getPost('productId');
		$product_json = json_decode($this->request->getPost('productJson'));
		$user_id = $this->user->user_auth_user_id;
		$data_user = $this->db->table('user')->select('*')->getWhere(['user_id' => $user_id])->getRow();

		if(empty($data_user)){
			$this->respondFailed("Data user tidak ditemukan.");
		}

		$username = $data_user->user_username;
		$user_fullname = $data_user->user_fullname;
		$datetime = date('Y-m-d H:i:s');

		$this->db->table('product_composition')->where('product_composition_master_id', $product_id)->delete();
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal mengubah resep.");
        }

		foreach($product_json as $product_recipe){
			$product_recipe_qty = $product_recipe->qty;
			$product_recipe_unit = $product_recipe->unit;
			$product_recipe_id = $product_recipe->recipe_id;
			$note = htmlspecialchars($product_recipe->note);

			$arr_data = [];
			$arr_data['product_composition_master_id'] = $product_id;
			$arr_data['product_composition_recipe_id'] = $product_recipe_id;
			$arr_data['product_composition_qty'] = $product_recipe_qty;
			$arr_data['product_composition_unit'] = $product_recipe_unit;
			$arr_data['product_composition_note'] = $note;
			$arr_data['product_composition_input_datetime'] = $datetime;
			$arr_data['product_composition_input_user_id'] = $user_id;
			$arr_data['product_composition_input_user_username'] = $username;
			$arr_data['product_composition_input_user_fullname'] = $user_fullname;

			if (!$this->db->table('product_composition')->insert($arr_data)) {
				$this->respondFailed("Gagal mengubah produk.");
			}
		}

		$this->respondSuccess("Berhasil mengubah resep.");
	}

	public function delete(){
		$this->validation->setRule('productId', 'Produk', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
		}

		$product_id = $this->request->getPost('productId');
		$this->db->table('product_composition')->where('product_composition_master_id', $product_id)->delete();
        if ($this->db->affectedRows() <= 0) {
            $this->respondFailed("Gagal menghapus resep.");
		}
		
		$arr_data_product = [];
		$arr_data_product['product_is_have_composition'] = 0;
		$this->db->table('product')->where('product_id', $product_id)->update($arr_data_product);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal menghapus resep.");
		}
		
		$this->respondSuccess("Berhasil menghapus resep.");
	}
}