<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AuthenticationApiFilter implements FilterInterface {

    public function before(RequestInterface $request, $arguments = null) {

        if ($request->getMethod() !== 'options') {
            $rest = \Config\Services::RestLib();

            $token = $rest->getBearerToken();

            if (empty($token)) {
                $rest->createRespond(401, array(
                    "status" => "unauthorized",
                    "msg" => "Anda tidak memiliki akses. Silahkan login.",
                    "result" => (object) array(),
                ));
            }
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null) {
        //todo
    }

}
